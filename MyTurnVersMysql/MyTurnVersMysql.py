# -*- coding: utf-8 -*-

from selenium import webdriver
import sys
import requests
import MySQLdb
import codecs
import pandas as pd
import os
import argparse
import configparser


def parse_args():
    '''Parse and return args.'''
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', nargs=1,
                        default='myturn-vers-mysql.conf', help='Chemin vers le fichier de configuration')
    parser.add_argument('--headless', action='store_true', help='Ne pas ouvrir de fenêtre Chrome graphique')
    return parser.parse_args()


class UserException(BaseException):
    def __init__(self):
        super().__init__()


class ConfigNotFoundException(UserException):
    def __init__(self, configfile):
        self._configfile = configfile

    def __str__(self):
        fmt = 'Impossible de lire le fichier de configuration {}.'
        return fmt.format(self._configfile)


class ConfigException(UserException):
    def __init__(self, section, name):
        super().__init__()
        self._section = section
        self._name = name

    def __str__(self):
        fmt = 'Impossible de trouver {}:{} dans le fichier de configuration.'
        return fmt.format(self._section, self._name)


def get_conf(config, section, name):
    if not config.has_option(section, name):
        raise ConfigException(section, name)

    return config.get(section, name)

def fixup_xml(xml):
    remplacement = {
        "First Name": "FirstName",
        "Last Name": "LastName",
        "Membre ID": "MembreID",
        "State/Province": "StateProvince",
        "Postal Code": "PostalCode",
        "Alt. Phone": "AltPhone",
        "Address Notes": "AddressNotes",
        "Secondary First Name": "SecondaryFirstName",
        "Secondary Last Name": "SecondaryLastName",
        "Secondary Email": "SecondaryEmail",
        "Secondary Organization": "SecondaryOrganization",
        "Secondary Address": "SecondaryAddress",
        "Secondary Address2": "SecondaryAddress2",
        "Secondary City": "SecondaryCity",
        "Secondary State/Province": "SecondaryStateProvince",
        "Secondary Postal Code": "SecondaryPostalCode",
        "Secondary Country": "SecondaryCountry",
        "Secondary Phone": "SecondaryPhone",
        "Secondary Alt. Phone": "SecondaryAltPhone",
        "Secondary Address Notes": "SecondaryAddressNotes",
        "Membre créé le (YYYY-MM-DD)": "DateCreation",
        "Start of first full membership (YYYY-MM-DD)": "DatePremierAbonnement",
        "Current Membership Type": "AbonnementActuel",
        "Latest Membership Change (request, upgrade, renewal, cancellation...) (YYYY-MM-DD)": "DateAbonnementDernierChangement",
        "Current Membership Expiration (YYYY-MM-DD)": "DateAbonnementActuelExpiration",
        "User Note": "UserNote",
        "User Warning": "UserWarning",
        "Household Type": "HouseholdType",
        "Household income range": "HouseholdIncomeRange",
        "Number of disabled people in household": "NumberOfDisabledPeopleInHousehold",
        "Renter/Homeowner": "RenterHomeowner",
        "Household Size": "HouseholdSize",
    }

    for (avant, apres) in remplacement.items():
        xml = xml.replace("<" + avant + ">", "<" + apres + ">")
        xml = xml.replace("</" + avant + ">", "</" + apres + ">")
        xml = xml.replace("<" + avant + " />", "<" + apres + " />")

    return xml

def captured_main():
    args = parse_args()

    if not os.path.isfile(args.config):
        raise ConfigNotFoundException(args.config)

    config = configparser.ConfigParser()
    config.read(args.config)

    myturn_username = get_conf(config, 'myturn', 'username')
    myturn_password = get_conf(config, 'myturn', 'password')
    mysql_host = get_conf(config, 'mysql', 'host')
    mysql_username = get_conf(config, 'mysql', 'username')
    mysql_password = get_conf(config, 'mysql', 'password')
    mysql_database = get_conf(config, 'mysql', 'database')

    opts = webdriver.ChromeOptions()
    if args.headless:
        opts.add_argument('headless')
    else:
        opts.add_argument('window-size=1200x600')

    driver = webdriver.Chrome(chrome_options=opts)

    # Charger la page de connexion et se connecter
    driver.get('https://montreal.myturn.com/library/myTurnLogin/auth')
    username_field = driver.find_element_by_name('j_username')
    username_field.send_keys(myturn_username)
    password_field = driver.find_element_by_name('j_password')
    password_field.send_keys(myturn_password)
    login_button = driver.find_element_by_css_selector('button[type=submit]')
    login_button.click()

    # Voler les cookies du browser et faire notre propre requête pour télécharger le XML
    jar = {}
    for cookie in driver.get_cookies():
        if 'myturn' in cookie['domain']:
            jar[cookie['name']] = cookie['value']
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    }
    url = 'https://montreal.myturn.com/library/orgMembership/exportUsers?'

    params = {
        'format': 'xml',
        'extension': 'xml',
        'membershipTypeId': '0',
    }

    r = requests.get(url, cookies=jar, headers=headers, params=params)
    r.encoding = 'utf-8'

    xml = r.text
    xml = fixup_xml(xml)

    with open('out.xml', 'w') as f:
        f.write(xml)

def main():
    try:
        captured_main()
    except UserException as e:
        print('Erreur: {}'.format(e))


if __name__ == '__main__':
    main()
