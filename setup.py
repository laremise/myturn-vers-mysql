from distutils.core import setup

setup(
    name="MyTurnVersMysql",
    version="0.1",
    packages=['MyTurnVersMysql'],
    install_requires=[
        "selenium",
        "requests",
        "mysqlclient",
        "pandas",
    ],
    entry_points={
        'console_scripts': ['myturn-vers-mysql=MyTurnVersMysql.MyTurnVersMysql:main'],
    }
)
